#!/bin/bash

change_submodule() {
    echo $RANDOM > submodule/bidou.txt
    git -C submodule add bidou.txt
    git -C submodule commit -m "Update bidou.txt"
    git -C submodule push
}

# Init
git checkout main
git submodule update
git -C submodule checkout main

# Create some new commits with LFS data in submodule
change_submodule
git -C submodule branch -f change1
change_submodule
git -C submodule branch -f change2

# Trigger a job on a branch with the submodule. This should work fine.
git add submodule
git commit -m "Update submodule to branch change2; this should work fine"
git push

# Trigger a job on a branch without the submodule.
git checkout branch-without-submodule
echo $RANDOM > whatever.txt
git add whatever.txt
git commit -m "Update whatever.txt"
git push

# Trigger a new job on a branch with the submodule, on a commit that the runner
# already has fetched, but which has never been checked out. Thus the LFS data
# has not been fetched. This will trigger the failure.
git checkout main
git submodule update
git -C submodule checkout change1
git add submodule
git commit -m "Update submodule to branch change1; this should trigger the bug"
git push
