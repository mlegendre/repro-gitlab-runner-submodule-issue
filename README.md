Reproduction for a gitlab-runner bug

## Setup

* fork this project and [repro-gitlab-runner-submodule-issue-submodule](https://gitlab.com/mlegendre/repro-gitlab-runner-submodule-issue-submodule)
* create a runner in this project's fork
* install gitlab-runner somewhere
* register the runner
* clone the repository, and `git submodule update --init`

## Trigger the bug

Run `./repro.sh`

It will create and push commits in branches `main` and `branch-without-submodule` so as to trigger a failure during checkout in the job of the CI pipeline created for the third push:

![screenshot](screenshot.png)

Example log:

```
Entering 'submodule'
warning: current Git remote contains credentials
batch response: Authentication required: Authorization error: https://gitlab-ci-token:64_QTjnmhm-fxyubfwR7mp2@gitlab.com/mlegendre/repro-gitlab-runner-submodule-issue-submodule.git/info/lfs/objects/batch
Check that you have proper access to the repository
Failed to fetch some objects from 'https://gitlab-ci-token:64_QTjnmhm-fxyubfwR7mp2@gitlab.com/mlegendre/repro-gitlab-runner-submodule-issue-submodule.git/info/lfs'
fatal: run_command returned non-zero status for submodule
```

What happens is that, in some circumstances, the credentials in the remote URL of the submodule may not be updated properly. I have written my understanding of the issue in https://gitlab.com/gitlab-org/gitlab-runner/-/issues/26993#note_1550153254
